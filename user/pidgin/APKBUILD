# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pidgin
pkgver=2.14.1
pkgrel=0
pkgdesc="Multi-protocol instant messaging client"
url="http://pidgin.im/"
arch="all"
license="GPL-2.0-only"
depends="gst-plugins-base"
makedepends="gtk+2.0-dev perl-dev libsm-dev startup-notification-dev
	libxml2-dev libidn-dev gnutls-dev dbus-dev dbus-glib-dev gstreamer-dev
	cyrus-sasl-dev ncurses-dev nss-dev tcl-dev tk-dev intltool gtkspell-dev
	gst-plugins-base-dev farstream-dev libgnt-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang finch libpurple"
source="https://downloads.sourceforge.net/pidgin/$pkgname-$pkgver.tar.bz2"

build() {
	LIBS="-lX11 -ltinfo" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-avahi \
		--enable-dbus \
		--disable-doxygen \
		--enable-gnutls \
		--disable-meanwhile \
		--disable-nm \
		--enable-perl \
		--disable-schemas-install \
		--disable-screensaver \
		--enable-tcl \
		--enable-vv \
		--enable-gstreamer \
		--enable-gstreamer-interfaces \
		--enable-farstream \
		--enable-cyrus-sasl \
		--enable-nss
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

finch() {
	pkgdesc="Text-based multi-protocol instant messaging client"
	mkdir -p "$subpkgdir"/usr/lib "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/finch "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/finch "$subpkgdir"/usr/bin/
}

libpurple() {
	pkgdesc="Multi-protocol instant messaging library"
	mkdir -p "$subpkgdir"/usr/lib "$subpkgdir"/usr/share/sounds
	mv "$pkgdir"/usr/lib/*purple* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/purple "$pkgdir"/usr/share/sounds \
		"$subpkgdir"/usr/share/
}

sha512sums="6ce5c58f32fa680d7f67e37a42b0f77c8253018cee21df2895d52166d9eb6ecaf0458b1610adbd46f337d00e75933db7578c2f9808654bd22838ba5db0a13030  pidgin-2.14.1.tar.bz2"
