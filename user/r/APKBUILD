# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=r
pkgver=3.6.2
pkgrel=0
pkgdesc="Environment for statistical computing and graphics"
url="https://www.r-project.org/"
arch="all"
options="!check"  # 20.482886 != 20.482887
license="GPL-2.0-only"
depends="cmd:which"
makedepends="byacc bzip2-dev cairo-dev curl-dev gfortran icu-dev libice-dev
	libtirpc-dev libx11-dev libxt-dev pango-dev pcre-dev pcre2-dev xz-dev
	zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://rweb.crmda.ku.edu/cran/src/base/R-3/R-$pkgver.tar.gz"
builddir="$srcdir/R-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--docdir=/usr/share/doc/R-$pkgver \
		--localstatedir=/var \
		--with-readline=no \
		--enable-R-shlib
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="17513e9f4dd27c61c11f7aa45227aeeeefb375bf5d4e193b471724f379a1b2da33e127cbe91aa175cbbbb048b892047e2f610280585c8159242a6c94790b07f9  R-3.6.2.tar.gz"
