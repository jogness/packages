# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kde-gtk-config
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE System Settings panel for configuring GTK+ application styles"
url="https://www.kde.org/"
arch="all"
license="GPL-3.0+"
depends="gsettings-desktop-schemas"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev gtk+2.0-dev gtk+3.0-dev
	karchive-dev kcmutils-dev kconfigwidgets-dev ki18n-dev kiconthemes-dev
	kio-dev knewstuff-dev gsettings-desktop-schemas-dev"
subpackages=""
source="https://download.kde.org/stable/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2ef81b454789136692fead9af2b8d7383805ed5cf91f9f781e637f65cbc293b9ac03be7583f5b5848c4db956da4212027c5b54a878c3796ff975292deef9aa84  kde-gtk-config-5.18.5.tar.xz"
