# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Martell Malone <martell@marinelayer.io>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=wine
pkgver=4.0.1
pkgrel=0
pkgdesc="A compatibility layer for running Windows programs"
url="http://www.winehq.com"
arch="pmmx x86 x86_64"
license="LGPL-2.0|"
# As of 2.0.3 most of the tests fails
options="!check"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc"
depends_dev="$pkgname perl"
makedepends="fontconfig-dev openldap-dev libxslt-dev libxxf86dga-dev
	libxcursor-dev libxrandr-dev libxdamage-dev mesa-dev flex-dev bison
	libpng-dev libjpeg-turbo-dev freetype-dev ncurses-dev openssl-dev
	libxcomposite-dev libxrender-dev libxinerama-dev zlib-dev
	cups-dev alsa-lib-dev libxml2-dev dbus-dev libxi-dev
	gnutls-dev tiff-dev v4l-utils-dev udisks2-dev autoconf automake
	openal-soft-dev pulseaudio-dev sane-dev eudev-dev mpg123-dev
	autoconf automake libtool"
source="http://dl.winehq.org/$pkgname/source/${pkgver%.[1-9]}/$pkgname-$pkgver.tar.xz
	dlclose.patch
	no-pie.patch
	printer.patch
	winhlp32-flex.patch
	"

prepare() {
	default_prepare
	aclocal && autoheader && autoreconf
}

build() {
	case "$CARCH" in
		x86_64) _win64=--enable-win64;;
	esac
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/usr/lib \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--with-cups \
		--with-curses \
		--with-dbus \
		--with-fontconfig \
		--with-freetype \
		--with-jpeg \
		--with-mpg123 \
		--with-openal \
		--with-opengl \
		--with-png \
		--with-pulse \
		--with-sane \
		--with-udev \
		--with-va \
		--with-x \
		--with-xcomposite \
		--with-xinerama \
		--with-xslt \
		$_win64
	make
}

check() {
	make test
}

package() {
	make -j1 DESTDIR="$pkgdir" install

	case "$CARCH" in
		x86_64) _wine="wine64";;
		*) _wine="wine";;
	esac

	install -Dm755 tools/wineapploader \
		"$pkgdir"/usr/bin/wineapploader

	for file in msiexec notepad regedit regsvr32 wineboot \
		winecfg wineconsole winefile winemine winepath; do
		rm "$pkgdir"/usr/bin/$file
		ln -sf /usr/bin/wineapploader "$pkgdir"/usr/bin/$file
	done
}

dev() {
	default_dev

	install -d "$subpkgdir"/usr/bin
	for file in widl wmc wrc winebuild winedump function_grep.pl \
		 winedbg winemaker winegcc winecpp wineg++; do
		mv "$pkgdir"/usr/bin/$file "$subpkgdir"/usr/bin/
	done

	install -d "$subpkgdir"/usr/lib/wine
	mv "$pkgdir"/usr/lib/wine/*.def "$subpkgdir"/usr/lib/wine/
}

doc() {
	default_doc
	rm -fr "$subpkgdir"/usr/share/man/*.UTF-8
}

libs() {
	default_libs

	install -d "$subpkgdir"/usr/lib/wine/fakedlls
	for ext in dll16 drv16 exe16 acm dll drv ds ocx sys tlb vxd; do
		for file in $(find "$pkgdir/usr/lib/wine/fakedlls" -name "*.$ext"); do
			mv "$pkgdir"/usr/lib/wine/fakedlls/${file##*/} \
				"$subpkgdir"/usr/lib/wine/fakedlls/
			mv "$pkgdir"/usr/lib/wine/${file##*/}.so \
				"$subpkgdir"/usr/lib/wine/
		done
	done
}

sha512sums="dd240525e389f841123a569fd2f9b8da698b7707bc0de408dfd60ae73c5e24d522abb42b76571c840df52c686fdeae56e0a9c69db2cd31f04cdb345feeacf1dc  wine-4.0.1.tar.xz
7e68ffc953f8858538b03d79b78b53b46e33dc01a474cf2eadaa726ff04918eb8675e80da1c82d2d61b12753f10fd6eda9114c5bc263fe373b6900f174bc246f  dlclose.patch
a96b1e248447912ba522067b67fc920063f355f326eaf3fec760c79a1c6e46019fbdcfa8b95b9d389959a7e3d3ab794f9aaf78913ad6a73be32b5d34ca8a2267  no-pie.patch
05dcc931a3bf5569f0314e3ede9df11a6f2a5806ed9ed66da094f3ee811af316046f177d5190c09007227a455e402b4dc747098fc82cd410574fb241cc5d53ba  printer.patch
522a94a31fc459e80ea7dd05f7aee64f6ae666ec05236d06614acde118d5c60002e0f253ae75edb5f02164f22937ca89578504b690d1a5611bd60f703c8f0c00  winhlp32-flex.patch"
