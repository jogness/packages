# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdewebkit
pkgver=5.74.0
pkgrel=0
pkgdesc="KDE integration with WebKit"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtwebkit-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev kcoreaddons-dev
	kio-dev kparts-dev kwallet-dev qt5-qttools-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdewebkit-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2363bbcf1d83ddc4ab30c80347c44a0c5ec1a3527772ee476ba488d5e58ae7ff6cba942e5da46d4400844ea56afdd116ffdb8693571753b7d50aa1b73ca53c0d  kdewebkit-5.74.0.tar.xz"
