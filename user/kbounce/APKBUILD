# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kbounce
pkgver=20.08.1
pkgrel=0
pkgdesc="Puzzle/arcade game to build walls"
url="https://games.kde.org/game.php?game=kbounce"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kwidgetsaddons-dev kdbusaddons-dev ki18n-dev
	kconfigwidgets-dev kcompletion-dev kxmlgui-dev kio-dev kdoctools-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbounce-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="32b867e9a4c95925a40aa53d51e3bbfde2e2d603386c453df86e8fdceeedb7cd8f1701950935d66be8932fbf8273840464bbc344393166924438bf4007a3b9bf  kbounce-20.08.1.tar.xz"
