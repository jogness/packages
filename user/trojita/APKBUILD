# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=trojita
pkgver=0.7
pkgrel=1
pkgdesc="Qt-based IMAP email client"
url="http://trojita.flaska.net/"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1 WITH Nokia-Qt-exception-1.1 OR GPL-3.0-only) AND BSD-3-Clause AND GPL-2.0-only AND GPL-2.0+ AND LGPL-2.0-only AND LGPL-2.0+ AND LGPL-2.1+"
depends=""
# Font required: https://github.com/qtwebkit/qtwebkit/issues/933
checkdepends="ttf-liberation"
makedepends="cmake extra-cmake-modules zlib-dev qt5-qtbase-dev qt5-qtwebkit-dev
	qt5-qttools-dev qt5-qtsvg-dev ragel mimetic-dev gpgme-dev
	qtkeychain-dev"
source="https://sourceforge.net/projects/trojita/files/src/trojita-$pkgver.tar.xz
	use-qgpgme.patch
	fix-gpg.patch
	CVE-2019-10734.patch
	CVE-2020-15047.patch
	"

# secfixes:
#   0.7-r1:
#    - CVE-2019-10734
#    - CVE-2020-15047

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DWITH_GPGMEPP=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# test_Html_formatting: requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E test_Html_formatting
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fe4d9316f97d913619f27d24a5023c3d8dd4a6b9fb058651be12c67188f394aa8cbb60c7593e5eb28fc12fc883b76deeeb5f4f631edd255fdec4c5862c9a91c8  trojita-0.7.tar.xz
740c2410d7236d722482f05dd1d2c681e35543620823cb7c1396710081f9de4f6ae530c5b5442ecf5d08a8e552f0697f3a35bf51e07a3b4336dec7021b665706  use-qgpgme.patch
9d0fbf7c0b0f6975990a7705f9d43043e5807401cee179d7a07f9514856332d6bb1aa8448e84d0083003c34a3bb181080b973e8c1f77d1b5a8930d07d57702da  fix-gpg.patch
db96a566924b5d7b80787ab624af3726d5dd3459653192436a377d6482ab73801a7dcca1df1b1d937cf0d0798b827e04f8ef2c1124f91dc9da3e8036ef61e28a  CVE-2019-10734.patch
2477612aca1e558fa3ba2b434a701cc255c573ac7e2001e7b5921c9b991f7c95720f53b70b49824e36bafb53ab53477950cb8d436e637fda4d59c7ec5883ce5f  CVE-2020-15047.patch"
