# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Gentoo Rust Maintainers <rust@gentoo.org>
# Contributor: Samuel Holland <samuel@sholland.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer:
pkgname=cbindgen
pkgver=0.14.2
pkgrel=0
pkgdesc="Tool to generate C bindings from Rust code"
url="https://github.com/eqrion/cbindgen"
arch="all"
license="MPL-2.0"
depends=""
makedepends="cargo"
source=""

# dependencies taken from Cargo.lock
cargo_deps="$pkgname-$pkgver
ansi_term-0.11.0
atty-0.2.13
bitflags-1.2.1
c2-chacha-0.2.3
cfg-if-0.1.10
clap-2.33.0
getrandom-0.1.13
heck-0.3.1
itoa-0.4.4
libc-0.2.66
log-0.4.8
ppv-lite86-0.2.6
proc-macro2-1.0.6
quote-1.0.2
rand-0.7.2
rand_chacha-0.2.1
rand_core-0.5.1
rand_hc-0.2.0
redox_syscall-0.1.56
remove_dir_all-0.5.2
ryu-1.0.2
serde-1.0.104
serde_derive-1.0.104
serde_json-1.0.44
strsim-0.8.0
syn-1.0.11
tempfile-3.1.0
textwrap-0.11.0
toml-0.5.5
unicode-segmentation-1.6.0
unicode-width-0.1.7
unicode-xid-0.2.0
vec_map-0.8.1
wasi-0.7.0
winapi-0.3.8
winapi-i686-pc-windows-gnu-0.4.0
winapi-x86_64-pc-windows-gnu-0.4.0
"

source="$source $(echo $cargo_deps | sed -E 's#([[:graph:]]+)-([0-9.]+(-(alpha|beta|rc)[0-9.]+)?)#&.tar.gz::https://crates.io/api/v1/crates/\1/\2/download#g')"

prepare() {
	export CARGO_HOME="$srcdir/cargo-home"
	export CARGO_VENDOR="$CARGO_HOME/adelie"

	(builddir=$srcdir; default_prepare)

	mkdir -p "$CARGO_VENDOR"
	cat <<- EOF > "$CARGO_HOME/config"
		[source.adelie]
		directory = "${CARGO_VENDOR}"

		[source.crates-io]
		replace-with = "adelie"
		local-registry = "/nonexistant"
	EOF

	for _dep in $cargo_deps; do
		ln -s "$srcdir/$_dep" "$CARGO_VENDOR/$_dep"
		_sum=$(sha256sum "$srcdir/$_dep.tar.gz" | cut -d' ' -f1)
		cat <<- EOF > "$CARGO_VENDOR/$_dep/.cargo-checksum.json"
		{
			"package":"$_sum",
			"files":{}
		}
		EOF
	done
}

build() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo build -j $JOBS --release
}

check() {
	export CARGO_HOME="$srcdir/cargo-home"

	# Failing tests
	rm -rf tests/rust/expand*
	cargo test -j $JOBS --release
}

package() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo install --path . --root="$pkgdir"/usr
	rm "$pkgdir"/usr/.crates.toml
}


sha512sums="d3ab0a4c217c9047bc3ada7911d4ae1bdfc09092835f78d232bb56ef0d9a499df43e358532dfb828a04df61a0cacccd7e57214cd15f466791eb1b8edd9f92ed0  cbindgen-0.14.2.tar.gz
a637466a380748f939b3af090b8c0333f35581925bc03f4dda9b3f95d338836403cf5487ae3af9ff68f8245a837f8ab061aabe57a126a6a2c20f2e972c77d1fa  ansi_term-0.11.0.tar.gz
4554ca7dedb4c2e8693e5847ef1fe66161ed4cb2c19156bb03f41ce7e7ea21838369dabaf447a60d1468de8bfbb7087438c12934c4569dde63df074f168569ad  atty-0.2.13.tar.gz
ad89b3798845e23737a620bba581c2ff1ff3e15bac12555c765e201d2c0b90ecea0cdbc5b5b1a3fa9858c385e8e041f8226f5acfae5bbbe9925643fff2bf3f0b  bitflags-1.2.1.tar.gz
302498f75d1befdb8bf8b61d0eff2762f0e86000c3909d819a377be2fec72e830bfb4530e24059906b499cef17ab56229f5627fb998ad9ba34a7c6c0de748538  c2-chacha-0.2.3.tar.gz
9d22616bfb4a75770a828a0a3cddac6787297a5fdc53eb17e25811cc94de717f2de8bd66d53c5d65ba1c83d8892aefee5ae758cf56a1ef0a0c3120f70b244339  cfg-if-0.1.10.tar.gz
f1075031414d48e4340bfe308904a95a31b72460724773c52a0bc8c004e625a04c904a39fc5420cb8c26a633321f9b5f4f69019c7aae5ed89900b63ed8c21a91  clap-2.33.0.tar.gz
534b01d28bc9e69a5ad1e84f34aa4154939c27d70c5f557f15e669fa8894c375486ee945a6a6d8adaf1b6b7accda0e24963f70e77dd51ee0638a6ae6daa38925  getrandom-0.1.13.tar.gz
134722192c46fd64b38d2fbec3a2511c560916e906491a72c4ec9d99b9f716dc58942fb99cac99b9a6ef2de5b5ad1d1c1155f292d091401b6873347d80c086d6  heck-0.3.1.tar.gz
f5e04bd908457e7592243ce64a99c5283428b767f4cc17d77946770411b06fccb0250625263c3e84a02a018ea7e8a0e4216e1929a71988bab8e1dbf603d3801d  itoa-0.4.4.tar.gz
5bf5645a7cbe626108cd71e1d189286ed161fcd3ea12cd34f4e392be4cf93bf78feb6128b1101bedc41a71091398f26771f3efd3880f779ee0be00f706914975  libc-0.2.66.tar.gz
0b71f97d5964134b5eea1332347e177806b2f171d0be5c410c0ff1539470b242ba9f0933fafd853e4171a43b5e373a150af18918924be431c7216022553a8a3b  log-0.4.8.tar.gz
b9764a841364eb89157c828a183fdf6aeeb63d1d35e4f5dcba79e5d8e20eee8f22c845636c5c7bc980da1d141d838528d5f190546b23aa5e4e51e626a0ddce3d  ppv-lite86-0.2.6.tar.gz
d0a6358eb6f9334128a5b47c096bfc57452e13d603c9514df433409069c00468fa2b3c76761d7f3696abb061c1c3ba24150e0cf9be2c5d571d580238d2d27853  proc-macro2-1.0.6.tar.gz
67778dff9dc5c4edcdd6454b74ad9353bb6c0c4e51c16cb82f2e393a7d7a0cde084d3c93279b718a8398c40af0a9377ebfae5321e69e635efd8390c125b75ce4  quote-1.0.2.tar.gz
d29d10e9788f0f44b56b2981aca01a115eca1018e6afe2428fca088cc9432f27d014c6f1f0c819317052f4c64a9c54cee8834eb08e081f292ad65160dcdd014c  rand-0.7.2.tar.gz
30933fdb94ca8d4bf040a7e08a42944a0d7c2f3f6a9a3d547e74bc32f922b0eb79d85afb1f6c85c78dc115170e70bdf96b36f0478d61ba5651876d5350ad18f6  rand_chacha-0.2.1.tar.gz
4f7500b35e165e6c817fdd67a50745d5497d24e554bb554705097e37258751e8755c4d6b8a69fcb5e1977708ba78620bc35d640e4e018fcd4e88d9dbdbebdcbf  rand_core-0.5.1.tar.gz
bca185612bed5cee4da76fb68fe854105da276f5bf2da464e596d586b925df798cc692ed881e276ab77c36b4b0551930966c93656be122ad05899d87853533b0  rand_hc-0.2.0.tar.gz
17a3044327aa733c830dd74e210d6bda32622617d1c5af9d70d7647232d8569ad0b04ccb14042cbabb5d97ad98e3f1f5a9968a75387354ffb4d175e9577115a1  redox_syscall-0.1.56.tar.gz
d19a45398a93adbcef9f233f6b3eaf4a63ae95f5bbae00c880b40c5edd34449e7f798ebcd4d11843c68ddfa15e11bed21d434b224e4a175dcb64ae011c13c8cd  remove_dir_all-0.5.2.tar.gz
c681e037f1eea488bad7fb5ecc79af52377bd4b9eeed95eb213201219d7f100702000f81947aff8f18641235fb62c138a30eb20c1f93ae518a4d9960a598bb64  ryu-1.0.2.tar.gz
638a1dacc158d69658a005e599adc19d6bc80d0c663f527b1ff906f729e70e027eacc4ea5b9b2c675ac990d5997f50f9d46d614ba95b0c73bb9606aba46b053a  serde-1.0.104.tar.gz
303c3d126ca56b0a7126a87e9e1be8bd60bdb212ed21b5e1aee015313fd769f1d260869c41b0335bd467fd4e3fc6bb26f428c7c4b35e9a55c009797cfa745d1e  serde_derive-1.0.104.tar.gz
ce1c68bfd2ceaa8b7f45cd34cfbc71dcfe3905b06fa47032403c54aa9eed0e618cd526938eb78dbc201e9480dbd64b4bfb405f2119478229f5cea12056dcf5e4  serde_json-1.0.44.tar.gz
1d55a8d946cd55f5f37d06aea536549ded95739fa58c0f2da285a0041154c181f663682bdcac643aa198b3e762d694a04f058db985c62ebe22b5c16327ba6d34  strsim-0.8.0.tar.gz
6b361c6cb27aebb3da5e81db270e7cd0fe8bfb11289c25ce46127214e222c2b64f53164c8869de41b77e9b780f967454ed5d7c5b73442ae292eadc4dea77494c  syn-1.0.11.tar.gz
a87ee51c36a81a8a8eb8f091eb57926682f38b707f7f641332d8752170e6c139a656ae49c6861f51e07c2fab5c86cc9b2ac158f5d89c6bff15d18934dd4e7ba5  tempfile-3.1.0.tar.gz
f5c0fe4f28ff1a3a0931e8e235b5157a45f67967985bcc752418c5ec3481fca44a8ae4800088889b37e8cd0533f53d3c456d5ffd19b767b3f83a87b49a2e209a  textwrap-0.11.0.tar.gz
8f6a04dc24ea775a9a28cc948039f20540ec983c46b287ebfa820e29cc5867803765ce099283f79d3b4a22c513b386603dbd8130808cb3354bc63b5d4706cb33  toml-0.5.5.tar.gz
80c6f92d9b39602a05809dc63a7cb05a305441689b6056801097a882a36c9795aabd660f884f707daa7e7fe6ad4983f6e245c226962cc982dbfeea2ddfddf41e  unicode-segmentation-1.6.0.tar.gz
39b8a539c9009d0421f54ae68b139f21456c9cb03d743b58535a977f98bc9655cf42eaacfadbcff796c187a6f315ae16259ee22be9c2da5aa042172c6b464d84  unicode-width-0.1.7.tar.gz
590f727d8d8354023062ae5fe7ac5bed1bcf79d86b883effd7f33b3ea3b1c8922998a63d621ca6962a969e890fa6edd009871f21cd57b1969264f41ba3f78359  unicode-xid-0.2.0.tar.gz
026cf10dc7ba98ae51dd312fc847cbaea41c25f0da5db6e0e22c2ecf75584bbf876d7bd96035fbbcf6696d702d5a3f25977e02a2d77cf519aa21e3ed05710e40  vec_map-0.8.1.tar.gz
1950e78df7f0ba21b917680633d092704f1fb906bd973de4ddc43cedb7bf449f6e881d50e3aa0d5595e8d58796915d582b69c116ef536f819b6f035affea18f0  wasi-0.7.0.tar.gz
5a899ee5f09f30d742b8b8eba78da05cd9f4c664408fdeb9370373f8756a962a23e3f1c07619e745b3270138606c9a369076c02c3f5353f657df09d203d9a736  winapi-0.3.8.tar.gz
a672ccefd0730a8166fef1d4e39f9034d9ae426a3f5e28d1f4169fa5c5790767693f281d890e7804773b34acdb0ae1febac33cde8c50c0044a5a6152c7209ec2  winapi-i686-pc-windows-gnu-0.4.0.tar.gz
4a654af6a5d649dc87e00497245096b35a2894ae66f155cb62389902c3b93ddcc5cf7d0d8b9dd97b291d2d80bc686af2298e80abef6ac69883f4a54e79712513  winapi-x86_64-pc-windows-gnu-0.4.0.tar.gz"
