# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgraphviewer
pkgver=2.4.3
pkgrel=1
pkgdesc="Graphviz DOT graph viewer"
url="https://www.kde.org/applications/graphics/kgraphviewer/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kparts-dev
	kcoreaddons-dev kdoctools-dev kio-dev kiconthemes-dev graphviz-dev
	kwidgetsaddons-dev boost-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kgraphviewer/$pkgver/kgraphviewer-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d18146ba380efe73e1ec716dd1cc310fe1eac23eccb10e2a699b107451637b9332cc25d62a4de762df4706cea182c9474ba1e885801c9832e1bb9bff1648e72d  kgraphviewer-2.4.3.tar.xz"
