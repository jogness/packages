# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kate
pkgver=20.08.1
pkgrel=0
pkgdesc="Advanced text editor with autocomplete, syntax highlighting, and more"
url="https://kate-editor.org/"
arch="all"
options="!check"  # All tests require X11.
license="(LGPL-2.1-only OR LGPL-3.0-only) AND GPL-3.0+"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev python3
	qt5-qtscript-dev kconfig-dev kcrash-dev ki18n-dev kjobwidgets-dev
	kio-dev kparts-dev ktexteditor-dev kwindowsystem-dev kxmlgui-dev
	kiconthemes-dev kdoctools-dev kwallet-dev kservice-dev kitemmodels-dev
	knewstuff-dev threadweaver-dev libgit2-dev kactivities-dev
	plasma-framework-dev"
subpackages="kwrite kwrite-doc:kwrite_doc kwrite-lang:kwrite_lang
	$pkgname-doc $pkgname-lang $pkgname-project"
source="https://download.kde.org/stable/release-service/$pkgver/src/kate-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

project() {
	pkgdesc="KDE Advanced Text Editor - Plugin for project management"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/ktexteditor/
	mv "$pkgdir"/usr/lib/qt5/plugins/ktexteditor/kateprojectplugin.so \
		"$subpkgdir"/usr/lib/qt5/plugins/ktexteditor/
}

kwrite() {
	pkgdesc="Simple GUI text editor"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/kwrite "$subpkgdir"/usr/bin/

	mkdir -p "$subpkgdir"/usr/share/metainfo
	mv "$pkgdir"/usr/share/metainfo/org.kde.kwrite.appdata.xml \
		"$subpkgdir"/usr/share/metainfo/

	mkdir -p "$subpkgdir"/usr/share/applications
	mv "$pkgdir"/usr/share/applications/org.kde.kwrite.desktop \
		"$subpkgdir"/usr/share/applications/
}

kwrite_doc() {
	pkgdesc="Simple GUI text editor (documentation)"
	install_if="docs kwrite=$pkgver-r$pkgrel"
	for _langpath in "$pkgdir"/usr/share/doc/HTML/*; do
		local _sublangpath="$subpkgdir/${_langpath#"$pkgdir"}"
		if [ -d "$_langpath"/kwrite ]; then
			mkdir -p "$_sublangpath"
			mv "$_langpath"/kwrite "$_sublangpath"/
		fi
	done
}

kwrite_lang() {
	pkgdesc="Simple GUI text editor (translations)"
	for _langpath in "$pkgdir"/usr/share/locale/*/LC_MESSAGES; do
		local _sublangpath="$subpkgdir/${_langpath#"$pkgdir"}"
		if [ -f "$_langpath"/kwrite.mo ]; then
			mkdir -p "$_sublangpath"
			mv "$_langpath"/kwrite.mo "$_sublangpath"/
		fi
	done
}

sha512sums="63d1c2d8371047abbdc3b1ea2e96b94a6f85ccec05ff5addc19f19d593e2b1760c91b4d87145d329516bd40e0bed740a94306df7760d1a2936edf09c87f6982a  kate-20.08.1.tar.xz"
