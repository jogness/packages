# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gnupg
pkgver=2.2.23
pkgrel=0
pkgdesc="Complete and free implementation of the OpenPGP standard"
url="https://www.gnupg.org/"
arch="all"
license="GPL-3.0+ AND GPL-2.0+ AND LGPL-2.1+ AND LGPL-3.0+ AND MIT AND BSD-3-Clause AND Public-Domain"
depends="pinentry"
makedepends="bzip2-dev gnutls-dev libassuan-dev libgcrypt-dev libgpg-error-dev
	libksba-dev libusb-dev npth-dev openldap-dev sqlite-dev zlib-dev"
pkggroups="gnupg"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-doc $pkgname-lang"
source="https://gnupg.org/ftp/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2
	0001-Include-sys-select.h-for-FD_SETSIZE.patch
	fix-i18n.patch
	60-scdaemon.rules"

# secfixes:
#   2.2.23-r0:
#     - CVE-2020-25125
#   2.2.19-r0:
#     - CVE-2019-14855
#   2.2.17-r0:
#     - CVE-2019-13050

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-nls \
		--enable-bzip2 \
		--enable-tofu \
		--enable-scdaemon \
		--enable-ccid-driver
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/lib/udev/rules.d
	install -Dm644 "$srcdir"/60-scdaemon.rules "$pkgdir"/lib/udev/rules.d
}

sha512sums="736b39628f7e4adc650b3f9937c81f27e9ad41e77f5345dc54262c91c1cf7004243fa7f932313bcde955e0e9b3f1afc639bac18023ae878b1d26e3c5a3cabb90  gnupg-2.2.23.tar.bz2
c6cc4595081c5b025913fa3ebecf0dff87a84f3c669e3fef106e4fa040f1d4314ee52dd4c0e0002b213034fb0810221cfdd0033eae5349b6e3978f05d08bcac7  0001-Include-sys-select.h-for-FD_SETSIZE.patch
b19a44dacf061dd02b439ab8bd820e3c721aab77168f705f5ce65661f26527b03ea88eec16d78486a633c474120589ec8736692ebff57ab9b95f52f57190ba6b  fix-i18n.patch
4bfb9742279c2d1c872d63cd4bcb01f6a2a13d94618eff954d3a37451fa870a9bb29687330854ee47e8876d6e60dc81cb2569c3931beaefacda33db23c464402  60-scdaemon.rules"
