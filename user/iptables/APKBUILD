# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer:
pkgname=iptables
pkgver=1.8.5
pkgrel=0
pkgdesc="Linux kernel firewall, NAT and packet mangling tools"
url="https://www.netfilter.org/projects/iptables/index.html"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
depends_dev="linux-headers"
# extra functionality may be enabled in the future if libmnl and
# libnetfilter_conntrack are packaged.
makedepends="$depends_dev libnftnl-dev bison flex autoconf automake"
subpackages="$pkgname-doc $pkgname-dev $pkgname-openrc" 
source="https://www.netfilter.org/projects/iptables/files/iptables-$pkgver.tar.bz2
	iptables.initd
	iptables.confd
	ip6tables.confd
	"

build() {
	export CFLAGS="$CFLAGS -D_GNU_SOURCE"
	./configure \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--sbindir=/sbin \
		--without-kernel \
		--disable-nftables \
		--enable-devel \
		--enable-libipq \
		--enable-shared

	# do not use rpath - iptables seems to use old libtool which requires
	# these hacks
	sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
	sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

	make
}

package() {
	make -j1 install DESTDIR="$pkgdir"

	mkdir -p "$pkgdir"/usr/include/libiptc \
		"$pkgdir"/usr/lib \
		"$pkgdir"/var/lib/iptables \
		"$pkgdir"/etc/iptables

	install -m644 include/iptables.h include/ip6tables.h \
		"$pkgdir"/usr/include/
	install include/libiptc/*.h "$pkgdir"/usr/include/libiptc/

	install -D -m755 "$srcdir"/iptables.initd "$pkgdir"/etc/init.d/iptables
	install -D -m755 "$srcdir"/iptables.initd "$pkgdir"/etc/init.d/ip6tables
	install -D -m644 "$srcdir"/iptables.confd "$pkgdir"/etc/conf.d/iptables
	install -D -m644 "$srcdir"/ip6tables.confd "$pkgdir"/etc/conf.d/ip6tables
}

sha512sums="6a6baa541bb7aa331b176e0a91894e0766859814b59e77c71351ac34d6ebd337487981db48c70e476a48c67bcf891cfc663221a7582feb1496ad1df56eb28da8  iptables-1.8.5.tar.bz2
726dfa1aa598d9dff802b99347138c861703b3dd28b2721a6b79b12f87d988369d3b4b9daee7cb2f9a930daac8921600de1e5342a347b3eae3791f0b9f0acbeb  iptables.initd
cb7fecd5cab2c78bd3f215a41f39ec11c37eb360efbe83982378a0e647e0aa9dc0b7ec915a5b5081aa2f7747464787e69404aa15ba15a063c32cb8fb7dd13d1e  iptables.confd
0897a7a22f8b700f7f1f5c355ad6cbf39740e44d6c962af99e479978d8a2d556ca7fe4e31f238829046b4a871ce0b5fd52e2544f1361d15dd1ea3e33992646c4  ip6tables.confd"
