# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdiamond
pkgver=20.08.1
pkgrel=0
pkgdesc="Three-in-a-row game"
url="https://games.kde.org/game.php?game=kdiamond"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev
	kconfig-dev kcrash-dev kdbusaddons-dev kdoctools-dev kwidgetsaddons-dev
	ki18n-dev kconfigwidgets-dev kxmlgui-dev knotifications-dev
	knotifyconfig-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdiamond-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="35f13586c943893acae1db2bccc0166193a1e4cab90b6d43a4b37264ed9bac5851cee838b3f8db7e1ae74b57068f67602849626abffc05bb06db8d61b5eaab7b  kdiamond-20.08.1.tar.xz"
