# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=accountsservice
pkgver=0.6.55
pkgrel=0
pkgdesc="D-Bus service for accessing user account information"
url="https://www.freedesktop.org/wiki/Software/AccountsService/"
arch="all"
license="GPL-3.0+ AND GPL-2.0+"
depends="dbus"
makedepends="cmake dbus-dev glib-dev gobject-introspection-dev meson ninja
	polkit-dev utmps-dev xmlto"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://www.freedesktop.org/software/accountsservice/accountsservice-$pkgver.tar.xz
	disable-msgfmt
	"

build() {
	CPPFLAGS="-D_PATH_WTMPX=\\\"/run/utmps/wtmp\\\"" meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		-Dadmin_group=wheel \
		-Ddocbook=true \
		-Dsystemd=false \
		-Dsystemdsystemunitdir=no \
		. output
	mkdir -p output/data
	# msgfmt(1) XML error
	cp data/org.freedesktop.accounts.policy.in \
		output/data/org.freedesktop.accounts.policy
	patch -Np1 < "$srcdir"/disable-msgfmt
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="c12e6a8e80f9b087f97238da4734d2d3a14a7c5cbd870a32a04b00116f176c818c39fb886f6dc72c3e93c136b0c2074ddf8f77e20431fa3bd54f138bea9d262d  accountsservice-0.6.55.tar.xz
218b6e28cd277365569c0a214276e7c40a41a3cfd6037a34b86b57dbab4273031089d91fa24191d9a9c30423b8dbb7d946ff62a564783d7f9d002125f9cf27e5  disable-msgfmt"
