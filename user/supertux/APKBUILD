# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=supertux
pkgver=0.6.2
pkgrel=0
pkgdesc="Platform game featuring Tux the Penugin"
url="https://www.supertux.org/"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="boost-dev cmake curl-dev glew-dev libogg-dev libvorbis-dev zlib-dev
	mesa-dev openal-soft-dev physfs-dev sdl2-dev sdl2_image-dev doxygen
	freetype-dev libpng-dev"
subpackages="$pkgname-doc"
source="https://github.com/SuperTux/supertux/releases/download/v$pkgver/SuperTux-v$pkgver-Source.tar.gz"
builddir="$srcdir"/SuperTux-v$pkgver-Source

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5baa783ee589b42a9bbce3740659dbb7b617ebfcc00c0a038c03d31b56700e3923c8548700ccebe42b325ca03bd85186bc5edef9f6580d93dc48d8aca88cbf74  SuperTux-v0.6.2-Source.tar.gz"
