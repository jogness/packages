# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rocs
pkgver=20.08.1
pkgrel=0
pkgdesc="Graph theory IDE"
url="https://www.kde.org/applications/education/rocs/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev boost-dev
	grantlee-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev karchive-dev
	kconfig-dev kcoreaddons-dev kcrash-dev kdeclarative-dev ki18n-dev
	kitemviews-dev ktexteditor-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/rocs-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f9bc679812caff5f1e1b299ab527cca6cab7f22ea57669bb17f58fe1c6ecb6275c7bb3444af82628ec5aae2ab43a426f51d3b5ca470e6440bd9b270755563e67  rocs-20.08.1.tar.xz"
