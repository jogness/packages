# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdcraw
pkgver=20.08.1
pkgrel=0
pkgdesc="RAW image file format support for KDE"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules libraw-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdcraw-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="93a687e361c3d2c6b16c8acbb197127bb614f2ed4a0d511cacb584c7f7d4e263e9f2e1e76ad8c1cf408779f99480f51b6c4278cd75c5833da720e1bb07d4f1f1  libkdcraw-20.08.1.tar.xz"
