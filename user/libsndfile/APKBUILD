# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libsndfile
pkgver=1.0.28
pkgrel=4
pkgdesc="Library for reading and writing sampled sound files (WAV, AIFF, ...)"
url="http://www.mega-nerd.com/libsndfile"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
subpackages="$pkgname-dev $pkgname-doc"
depends=""
depends_dev="flac-dev libogg-dev libvorbis-dev"
makedepends="$depends_dev alsa-lib-dev linux-headers"
source="http://www.mega-nerd.com/$pkgname/files/$pkgname-$pkgver.tar.gz
	CVE-2017-8361_CVE-2017-8363_CVE-2017-8365.patch
	CVE-2017-8362.patch
	CVE-2017-12562.patch
	varargs-32bit.patch
	"

# secfixes:
#   1.0.28-r2:
#   - CVE-2017-12562
#   1.0.28-r0:
#   - CVE-2017-7585
#   - CVE-2017-7741
#   - CVE-2017-7742
#   1.0.28-r1:
#   - CVE-2017-8361
#   - CVE-2017-8362
#   - CVE-2017-8363
#   - CVE-2017-8365

build() {
	ac_cv_sys_largefile_CFLAGS="-D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-sqlite \
		--enable-largefile
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="890731a6b8173f714155ce05eaf6d991b31632c8ab207fbae860968861a107552df26fcf85602df2e7f65502c7256c1b41735e1122485a3a07ddb580aa83b57f  libsndfile-1.0.28.tar.gz
f98c40696fca3e7bca867df993de55bb4145c23428e65d1a669182eb2293046478ac727ae7f94bb77123ef0355c3c53be4f9d6a432665c90c74687d8d3afd9e3  CVE-2017-8361_CVE-2017-8363_CVE-2017-8365.patch
dfd4b5f1c7471fc416eed5c6040580a020543f145de9103751adaad6ce1c5c6a22abc1cf0ffd381aed3072644cd5ee03ba3598265aa7d202d63167da251cb595  CVE-2017-8362.patch
814139567d90fb07908014e858c341fe933e04dca69b88ad66078910888237bbeba94f85d9e1489883c424f35fca312eb98c21ae2b122d9289bb6418725cd02e  CVE-2017-12562.patch
2b83bacec23665cd31a596a1ce1fb543f935c7609dfff93a85822f81d66b3483cd547cd043eefb901d543276c270a17add70bf0db6348b5279220a7ecbd8b339  varargs-32bit.patch"
