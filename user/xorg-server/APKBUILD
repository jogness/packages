# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xorg-server
pkgver=1.20.9
pkgrel=0
pkgdesc="X.Org X11 server"
url="https://www.X.Org/"
arch="all"
options="suid"
license="MIT"
depends="font-cursor-misc font-misc-misc xkeyboard-config xkbcomp xinit"
depends_dev="libepoxy-dev libxfont2-dev mesa-dev"
makedepends="$depends_dev autoconf automake libtool util-macros
	eudev-dev libpciaccess-dev libdrm-dev libepoxy-dev pixman-dev
	libx11-dev libxdamage-dev libxinerama-dev libxkbfile-dev libxkbui-dev
	libxv-dev libxxf86dga-dev libxxf86misc-dev xcb-util-dev
	xcb-util-image-dev xcb-util-keysyms-dev xcb-util-renderutil-dev
	xcb-util-wm-dev xorgproto-dev
	xtrans
	openssl-dev perl zlib-dev
	"
# the modesetting driver is now shipped with xorg server
replaces="xf86-video-modesetting"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc xvfb $pkgname-xephyr
	$pkgname-xnest"
source="https://www.X.Org/releases/individual/xserver/$pkgname-$pkgver.tar.bz2
	autoconfig-sis.patch
	fix-musl-arm.patch
	"

# secfixes:
#   1.20.9-r0:
#     - CVE-2020-14345
#     - CVE-2020-14346
#     - CVE-2020-14347
#     - CVE-2020-14361
#     - CVE-2020-14362
#   1.20.3-r0:
#     - CVE-2018-14665
#   1.19.5-r0:
#     - CVE-2017-12176
#     - CVE-2017-12177
#     - CVE-2017-12178
#     - CVE-2017-12179
#     - CVE-2017-12180
#     - CVE-2017-12181
#     - CVE-2017-12182
#     - CVE-2017-12183
#     - CVE-2017-12184
#     - CVE-2017-12185
#     - CVE-2017-12186
#     - CVE-2017-12187
#     - CVE-2017-13721
#     - CVE-2017-13723

prepare() {
	default_prepare

	# Fix dbus config path
	sed -i -e 's/\$(sysconfdir)/\/etc/' config/Makefile.*
	sed -i -e 's/termio.h/termios.h/' hw/xfree86/os-support/xf86_OSlib.h
}

build() {
	export CFLAGS="$CFLAGS -D_GNU_SOURCE"
	[ "$CLIBC" = musl ] && export CFLAGS="$CFLAGS -D__gid_t=gid_t -D__uid_t=uid_t"
	export LDFLAGS="$LDFLAGS -Wl,-z,lazy"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc/X11 \
		--localstatedir=/var \
		--with-xkb-path=/usr/share/X11/xkb \
		--with-xkb-output=/var/lib/xkb \
		--without-systemd-daemon \
		--enable-composite \
		--enable-config-udev \
		--enable-dri \
		--enable-dri2 \
		--enable-glamor \
		--enable-ipv6 \
		--enable-kdrive \
		--enable-xace \
		--enable-xcsecurity \
		--enable-xephyr \
		--enable-xnest \
		--enable-xorg \
		--enable-xres \
		--enable-xv \
		--disable-xwayland \
		--disable-config-hal \
		--disable-dmx \
		--disable-systemd-logind \
		--with-os-vendor="${DISTRO_NAME:-Adélie Linux}"

	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	chmod u+s "$pkgdir"/usr/bin/Xorg

	# Don't conflict with xf86-input-evdev
	rm -f "$pkgdir"/usr/share/X11/xorg.conf.d/10-evdev.conf

	install -m755 -d "$pkgdir"/etc/X11/xorg.conf.d
	install -m755 -d "$pkgdir"/var/lib/xkb
	install -m644 -D COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

xvfb() {
	pkgdesc="X.Org server for virtual framebuffer (for testing)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xvfb "$subpkgdir"/usr/bin/
}

xephyr() {
	pkgdesc="kdrive-based X11 server (windowed framebuffer on X11 server)"
	depends=
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xephyr "$subpkgdir"/usr/bin/
}

xnest() {
	pkgdesc="X.Org nested server"
	depends=
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xnest "$subpkgdir"/usr/bin/
}

xwayland() {
	pkgdesc="X.Org server for Wayland"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xwayland "$subpkgdir"/usr/bin/
}

sha512sums="d9b5f93e1b9763a89187d8b272aa7d4ce9709641b8539f4536708af153310e5a4931bffd4229c51a3b0e3b12da7838750aa71b635751fb4c0bb27438cce4e5e6  xorg-server-1.20.9.tar.bz2
d77151bc51766e1230a121c008ac1d0695275bf889b1db4b3330c1f8ee720b0e046cc935fa14aaef40b02fdea508e84e53959b560131ace14ace14943c8eb734  autoconfig-sis.patch
a5f910e72ff9abd4e4a0c6806cdbe48d1b0b6cc0586f36568da5864a8dedc46a3112fe86d7a1969033f4d5b0def4dc6e5c11b656fbcc964732b417e6c9577f22  fix-musl-arm.patch"
