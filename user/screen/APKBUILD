# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=screen
pkgver=4.8.0
pkgrel=0
pkgdesc="A window manager that multiplexes a physical terminal"
url="http://ftp.gnu.org/gnu/screen/"
arch="all"
options="!check"  # No test suite.
license="GPL-3.0+"
depends=""
makedepends="ncurses-dev utmps-dev libutempter-dev"
subpackages="$pkgname-doc"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz
	utmpx.patch
	"

build() {
	export CFLAGS="-DNONETHACK -DGETUTENT"
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-colors256
	make
}

package() {
	make -j1 DESTDIR="$pkgdir" install

	# remove suid root
	find "$pkgdir" -type f -perm -u+s -print0 \
		| xargs -0 chmod -s

	install -Dm644 etc/etcscreenrc "$pkgdir"/etc/screenrc
	install -Dm644 etc/screenrc "$pkgdir"/etc/skel/.screenrc
}

sha512sums="770ebaf6ee9be711bcb8a6104b3294f2bf4523dae6683fdc5eac4b3aff7e511be2d922b6b2ad28ec241113c2e4fe0d80f9a482ae1658adc19c8c3a3680caa25c  screen-4.8.0.tar.gz
82aca3e16c8cd7a3029d3b589ff2dd3471708d6287979ebb5cfdaedbd1f3012c0cd660b131e0cbe142b99786e49ef8b24c63159523d870e95ccf71ec94b82634  utmpx.patch"
