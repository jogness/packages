# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=spice-gtk
pkgver=0.38
pkgrel=1
pkgdesc="A GTK+ widget for SPICE clients"
url="https://www.spice-space.org/"
arch="all"
# suid: ACL helper for USB redirection
options="suid"
license="LGPL-2.1+ AND LGPL-2.0+ AND BSD-3-Clause AND MIT AND GPL-3.0+ AND LGPL-2.0-only AND GPL-2.0+"
depends="gst-plugins-good"
depends_dev="gobject-introspection-dev gtk+3.0-dev"
makedepends="$depends_dev acl-dev bash cyrus-sasl-dev eudev-dev
	gst-plugins-base-dev gstreamer-dev gstreamer-tools gtk-doc
	json-glib-dev libjpeg-turbo-dev
	libxrandr-dev lz4-dev meson openssl-dev opus-dev polkit-dev
	py3-pyparsing py3-six spice-protocol
	zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang spicy
	spice-glib:glib"
source="https://www.spice-space.org/download/gtk/$pkgname-$pkgver.tar.xz
	macro.patch
	"

build() {
	# Note: pulseaudio support is disabled because it's deprecated.
	# Audio is still supported through gstreamer.
	#
	# USB redirection is disabled until there is reasonable belief
	# that it is endian safe.
	# https://gitlab.freedesktop.org/spice/spice-gtk/-/issues/120
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		-Dcelt051=disabled \
		-Dpulse=disabled \
		-Dusbredir=disabled \
		-Dcoroutine=gthread \
		. output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

spicy() {
	pkgdesc="SPICE client (remote virtual machine access)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

glib() {
	pkgdesc="$pkgdesc (GLib libraries)"
	mkdir -p "$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/lib/girepository-1.0/
	mv "$pkgdir"/usr/lib/*-glib-*.so* \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/girepository-1.0/SpiceClientGLib-*.typelib \
		"$subpkgdir"/usr/lib/girepository-1.0/
}

sha512sums="27b44ac9f0cee2737ce03bb3f47c62fc0ee2402c291c49fc56cffc4ccb63e2cab001a68ba865a6375d82cb38444408d59c68469783ee4279fa818d8682e902f3  spice-gtk-0.38.tar.xz
9bae6a1866c977d6b7e032e0fe1f8ce07ccfd8777e982453629d9dce284a6f02c68ab8b073865e3c36340e22dd2f83fbda4cabd3a5bc8a274b3a0bbf9c031ccd  macro.patch"
