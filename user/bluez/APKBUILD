# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bluez
pkgver=5.55
pkgrel=0
pkgdesc="Linux Bluetooth protocol stack"
url="http://www.bluez.org/"
arch="all"
license="GPL-2.0+"
depends="dbus elogind"
makedepends="alsa-lib-dev dbus-dev eudev-dev glib-dev libical-dev
	linux-headers ncurses-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-bccmd
	$pkgname-btmgmt $pkgname-btmon $pkgname-cups $pkgname-deprecated
	$pkgname-hid2hci $pkgname-obexd $pkgname-openrc $pkgname-tools"
source="https://www.kernel.org/pub/linux/bluetooth/bluez-$pkgver.tar.xz
	https://ftp.gnu.org/gnu/readline/readline-8.0.tar.gz
	bluetooth.initd
	rfcomm.initd
	rfcomm.confd
	001-bcm43xx-Add-bcm43xx-3wire-variant.patch
	002-bcm43xx-The-UART-speed-must-be-reset-after-the-firmw.patch
	003-Increase-firmware-load-timeout-to-30s.patch
	004-Move-the-43xx-firmware-into-lib-firmware.patch
	dbus-without-systemd.patch
	mesh-without-systemd.patch
	obex-without-systemd.patch
	disable-lock-test.patch
	fix-endianness.patch
	time64.patch
	"

# secfixes:
#   5.55-r0:
#     - CVE-2020-27153
#   5.54-r0:
#     - CVE-2020-0556

prepare() {
	default_prepare

	cd "$srcdir"/readline-8.0
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix="$srcdir"/readline \
		--enable-static \
		--disable-shared
	make install
}

build() {
	CFLAGS="$CFLAGS -I$srcdir/readline/include -L$srcdir/readline/lib" \
	CPPFLAGS="$CPPFLAGS -I$srcdir/readline/include" LIBS="-ltinfo" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--libexecdir=/usr/lib \
		--disable-systemd \
		--enable-client \
		--enable-library \
		--enable-deprecated \
		--enable-experimental \
		--enable-tools \
		--enable-hid2hci
	make
}

check() {
	make check
}

package() {
	make install DESTDIR="$pkgdir"
	install -D -m644 src/main.conf "$pkgdir"/etc/bluetooth/main.conf

	install -Dm755 "$srcdir"/bluetooth.initd "$pkgdir"/etc/init.d/bluetooth
	install -Dm755 "$srcdir"/rfcomm.initd "$pkgdir"/etc/init.d/rfcomm
	install -Dm644 "$srcdir"/rfcomm.confd "$pkgdir"/etc/conf.d/rfcomm
	install -Dm755 test/simple-agent "$pkgdir"/usr/bin/bluez-simple-agent
}

bccmd() {
	pkgdesc="BlueZ utility for the CSR BCCMD interface"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/bccmd "$subpkgdir"/usr/bin/
}

btmon() {
	pkgdesc="BlueZ Bluetooth monitor"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/btmon "$subpkgdir"/usr/bin/
}

btmgmt() {
	pkgdesc="Bluez tool for the Bluetooth Management API"
	install -Dm755 "$builddir"/tools/btmgmt -t \
		"$subpkgdir"/usr/bin
}

cups() {
	pkgdesc="BlueZ backend for CUPS"
	depends="cups"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/cups "$subpkgdir"/usr/lib/
}

hid2hci() {
	pkgdesc="Put HID proxying Bluetooth HCIs into HCI mode"
	replaces="bluez"
	mkdir -p "$subpkgdir"
	mv "$pkgdir"/lib "$subpkgdir"/
}

deprecated() {
	pkgdesc="Deprecated Bluetooth tools"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ciptool \
		"$pkgdir"/usr/bin/hciattach \
		"$pkgdir"/usr/bin/hciconfig \
		"$pkgdir"/usr/bin/hcidump \
		"$pkgdir"/usr/bin/hcitool \
		"$pkgdir"/usr/bin/rfcomm \
		"$pkgdir"/usr/bin/sdptool \
		"$subpkgdir"/usr/bin/
}

obexd() {
	pkgdesc="BlueZ OBEX daemon"
	mkdir -p "$subpkgdir"/usr/lib/bluetooth
	mv "$pkgdir"/usr/lib/bluetooth/obexd "$subpkgdir"/usr/lib/bluetooth
}

tools() {
	pkgdesc="Bluetooth tools"
	mkdir -p "$subpkgdir"/usr/bin
	for _tool in bluetooth-player mpris-proxy; do
		install -Dm755 "$builddir"/tools/$_tool "$subpkgdir"/usr/bin/$_tool
	done
}

sha512sums="9423cb60d15a6f068838497a1eaea9f5a32d70c07191c313ba821a6919d6e0c436ada4f547cc5f2db5eacc0123429ad54851f57df2554f61fa293743ec14a033  bluez-5.55.tar.xz
41759d27bc3a258fefd7f4ff3277fa6ab9c21abb7b160e1a75aa8eba547bd90b288514e76264bd94fb0172da8a4faa54aab2c07b68a0356918ecf7f1969e866f  readline-8.0.tar.gz
fc43c78ed248ea412529eed5ae8bb47bacca9bf5b3b10de121ddd4e792c85893561a88be4aa2c6318106e5d2146a721445152d44fa60ca257ca0b4eb87318c1e  bluetooth.initd
8d7b7c8938a2316ce0a855e9bdf1ef8fcdf33d23f4011df828270a088b88b140a19c432e83fef15355d0829e3c86be05b63e7718fef88563254ea239b8dc12ac  rfcomm.initd
a70aa0dbbabe7e29ee81540a6f98bf191a850da55a28f678975635caf34b363cf4d461a801b3484120ee28fdd21240bd456a4f5d706262700924bd2e9a0972fb  rfcomm.confd
73202915fda01d420b2864da77b1c25af5a55c815e9be6546400a0940bfb6097d83322790bc22a80ec0fcd557144fdd1877e243a79285a7f040ff96ba3600b94  001-bcm43xx-Add-bcm43xx-3wire-variant.patch
d5fd1c962bd846eaa6fff879bab85f753eb367d514f82d133b5d3242e1da989af5eddd942c60a87d5b67783e060f91bfa0f74fb1e8e6699cdee6e5bbe6a431ea  002-bcm43xx-The-UART-speed-must-be-reset-after-the-firmw.patch
784e9644c8de4e2693e2eeed988a245608b8cb14e1fc0dff8795c60c527b2e8d0c87862cfbfd6b850b47ae80cdf993a5ed3f477078ea1068fd7374899c7a1a77  003-Increase-firmware-load-timeout-to-30s.patch
42ac04044a8c66e07487598b3a75ef52efc32999ebce4e7c63f6198e2f603f4a1442e74600e43a0938cb4f52d4db0298aa99050b18144b84990cda71748e9de5  004-Move-the-43xx-firmware-into-lib-firmware.patch
a681a98178f9374ab882984058559752d1e1f3261412f450b38f1a06c3dfb5ce45e2cce35aa152f3c5761a9f7695fb0990cfdcc394706304470e6d1a8dbef682  dbus-without-systemd.patch
59fa8b90d5b8ac5922b730b01fbeb8a237075cd537705fe2665f15c7626bc6a8bd8128a3e899b0c620b6cb4f86862c2a416b2af8c2575d0724b26c8132aff251  mesh-without-systemd.patch
9aa78fdd9a84dd2a20cd6a4f5105e88af16896ae81a76a9a9f86902a9988d58af1102948c2d87397df36d747ce206d06a7bf1795c3ceb6a4617d95a26be3526f  obex-without-systemd.patch
04c4889372c8e790bb338dde7ffa76dc32fcf7370025c71b9184fcf17fd01ade4a6613d84d648303af3bbc54043ad489f29fc0cd4679ec8c9029dcb846d7e026  disable-lock-test.patch
118d55183860f395fc4bdc93efffb13902ebf7388cad722b9061cd2860d404333e500af521741c3d92c0f8a161f6810348fbeb6682e49c372383f417aed8c76a  fix-endianness.patch
da2ca90b0de1afb90914370dbc482f78ab9a270e60142ed7901eff408d02611d7fe423ae4677bce49fe829d59aa9aa971f9d0039d6e9134d3346aeb51b62ba9c  time64.patch"
