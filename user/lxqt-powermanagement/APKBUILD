# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=lxqt-powermanagement
pkgver=0.15.0
_lxqt_build=0.7.0
pkgrel=0
pkgdesc="Power management utilities for LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt_build upower-dev
	liblxqt-dev>=${pkgver%.*}.0 kidletime-dev qt5-qtsvg-dev qt5-qttools-dev
	solid-dev kwindowsystem-dev"
source="https://github.com/lxqt/lxqt-powermanagement/releases/download/$pkgver/lxqt-powermanagement-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="8ea0765bad3885e6a8e574b382d6bea0bd5065b760241b5fa641e17096fffb43c5472577729365c36918caa4ca0d058fa1a24538dfe157e32ac519d94a4db039  lxqt-powermanagement-0.15.0.tar.xz"
