# Contributor: G.J.R. Timmer <gjr.timmer@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=postgresql
pkgver=10.12
pkgrel=0
pkgdesc="Featureful object-relational database system (RDBMS)"
url="https://www.postgresql.org/"
arch="all"
options="!checkroot"
license="PostgreSQL"
pkgusers="postgres"
pkggroups="postgres"
depends="postgresql-client tzdata"
depends_dev="openssl-dev"
makedepends="$depends_dev libedit-dev zlib-dev libxml2-dev util-linux-dev
	openldap-dev tcl-dev perl-dev python3-dev"
checkdepends="diffutils"
subpackages="$pkgname-contrib $pkgname-dev $pkgname-doc libpq $pkgname-libs
	$pkgname-client $pkgname-pltcl
	$pkgname-plperl $pkgname-plperl-contrib:plperl_contrib
	$pkgname-plpython3 $pkgname-plpython3-contrib:plpython3_contrib"
install="$pkgname.pre-upgrade"
source="https://ftp.postgresql.org/pub/source/v$pkgver/$pkgname-$pkgver.tar.bz2
	perl-rpath.patch
	conf-unix_socket_directories.patch
	disable-broken-tests.patch
	$pkgname.initd
	$pkgname.confd
	pg-restore.initd
	pg-restore.confd
	pltcl_create_tables.sql
	"

# secfixes:
#   9.6.4-r0:
#     - CVE-2017-7546
#     - CVE-2017-7547
#     - CVE-2017-7548
#   9.6.3-r0:
#     - CVE-2017-7484
#     - CVE-2017-7485
#     - CVE-2017-7486
#   10.1-r0:
#     - CVE-2017-15098
#     - CVE-2017-15099
#   10.2-r0:
#     - CVE-2018-1052
#     - CVE-2018-1053
#   10.3-r0:
#     - CVE-2018-1058
#   10.4-r0:
#     - CVE-2018-1115
#   10.8-r0:
#     - CVE-2018-16850
#     - CVE-2019-10130
#   10.9-r0:
#     - CVE-2019-10164
#   10.10-r0:
#     - CVE-2019-10208

prepare() {
	default_prepare

	for file in $pkgname.initd $pkgname.confd; do
		sed "s|@VERSION@|${pkgver%.*}|" "$srcdir"/$file > $file
	done
}

build() {
	export PYTHON=/usr/bin/python3
	export CFLAGS="$(printf '%s' "$CFLAGS" | sed 's/-Os/-O2/')"
	export CFLAGS="$(printf '%s' "$CPPFLAGS" | sed 's/-Os/-O2/')"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--with-system-tzdata=/usr/share/zoneinfo \
		--with-ldap \
		--with-libedit-preferred \
		--with-libxml \
		--with-openssl \
		--with-uuid=e2fs \
		--disable-rpath \
		--with-perl \
		--with-python \
		--with-tcl
	make world
}

check() {
	_run_tests src/test
	_run_tests src/pl
	_run_tests contrib
}

package() {
	make DESTDIR="$pkgdir" install install-docs

	cd "$pkgdir"

	install -d -m750 -o postgres -g postgres \
		./var/lib/postgresql \
		./var/log/$pkgname

	install -D -m755 "$builddir"/postgresql.initd ./etc/init.d/postgresql
	install -D -m644 "$builddir"/postgresql.confd ./etc/conf.d/postgresql

	install -D -m755 "$srcdir"/pg-restore.initd ./etc/init.d/pg-restore
	install -D -m644 "$srcdir"/pg-restore.confd ./etc/conf.d/pg-restore
}

dev() {
	default_dev

	_submv usr/bin/pg_config \
		usr/bin/ecpg \
		usr/lib/postgresql/pgxs
}

libpq() {
	pkgdesc="PostgreSQL libraries"
	depends=""

	_submv usr/lib/libpq.so.*
}

libs() {
	depends=""
	default_libs
}

client() {
	pkgdesc="PostgreSQL client"
	depends=""

	cd "$pkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/bin
	mv clusterdb \
		createdb \
		createuser \
		dropdb \
		dropuser \
		pg_basebackup \
		pg_dump \
		pg_dumpall \
		pg_isready \
		pg_receivewal \
		pg_recvlogical \
		pg_restore \
		psql \
		reindexdb \
		vacuumdb \
		"$subpkgdir"/usr/bin/
}

contrib() {
	pkgdesc="Extension modules distributed with PostgreSQL"
	depends=""

	cd "$builddir"
	# Avoid installing plperl and plpython extensions, these will be
	# installed into separate subpackages.
	sed -Ei -e 's/(.*_plperl)/#\1/' \
		-e 's/(.*_plpython)/#\1/' \
		contrib/Makefile

	make -C contrib DESTDIR="$subpkgdir" install

	mv "$subpkgdir"/usr/share/doc/postgresql/extension \
		"$pkgdir"/usr/share/doc/postgresql/
	rmdir -p "$subpkgdir"/usr/share/doc/postgresql || true
}

pltcl() {
	pkgdesc="PL/Tcl procedural language for PostgreSQL"
	depends="pgtcl"

	_submv usr/lib/postgresql/pltcl.so \
		usr/share/postgresql/extension/pltcl*

	install -m 644 "$srcdir"/pltcl_create_tables.sql \
		"$subpkgdir"/usr/share/postgresql/
}

plperl() {
	pkgdesc="PL/Perl procedural language for PostgreSQL"
	depends=""

	_submv usr/lib/postgresql/plperl.so \
		usr/share/postgresql/extension/plperl*
}

plperl_contrib() {
	_plcontrib plperl "PL/Perl"

	cd "$builddir"
	make -C contrib/hstore_plperl DESTDIR="$subpkgdir" install
}

plpython3() {
	pkgdesc="PL/Python3 procedural language for PostgreSQL"
	depends="python3"

	cd "$builddir"
	make -C src/pl/plpython DESTDIR="$subpkgdir" install
	rm -R "$subpkgdir"/usr/include
}

plpython3_contrib() {
	_plcontrib plpython3 "PL/Python 3"

	cd "$builddir"
	make -C contrib/hstore_plpython DESTDIR="$subpkgdir" install
	make -C contrib/ltree_plpython DESTDIR="$subpkgdir" install

	cd "$subpkgdir"/usr/share/postgresql/extension/
	rm *plpython2* *plpythonu*
}

_plcontrib() {
	local subname="$1"
	pkgdesc="$2 extension modules distributed with PostgreSQL"
	depends="$pkgname-$subname"
	install_if="$pkgname-$subname=$pkgver-r$pkgrel $pkgname-contrib=$pkgver-r$pkgrel"
}

_run_tests() {
	local path="$1"; shift

	msg "Running test suite at $path..."
	# Note: some tests fail when running in parallel.
	make -k -j 1 -C "$path" $@ check MAX_CONNECTIONS=5 || {
		printf "\n%s\n\n" "Trying to find all regression.diffs files in build directory..." >&2
		find "$path" -name regression.diffs | while read file; do
			echo "=== test failure: $file ===" >&2
			cat "$file" >&2
		done
		return 1
	}
}

_submv() {
	for path in "$@"; do
		mkdir -p "$subpkgdir/${path%/*}"
		mv "$pkgdir"/$path "$subpkgdir"/${path%/*}/
	done
}

sha512sums="6accc66cbbae811509095c33e8a8d17ddd11d9e307267312e3d09df90469db4700a5806166d66f25d77769d3ef88653c98dfc7d05dd053f10434b03e0a9e33b7  postgresql-10.12.tar.bz2
5f9d8bb4957194069d01af8ab3abc6d4d83a7e7f8bd7ebe1caae5361d621a3e58f91b14b952958138a794e0a80bc154fbb7e3e78d211e2a95b9b7901335de854  perl-rpath.patch
8439a6fdfdea0a4867daeb8bc23d6c825f30c00d91d4c39f48653f5ee77341f23282ce03a77aad94b5369700f11d2cb28d5aee360e59138352a9ab331a9f9d0f  conf-unix_socket_directories.patch
1966b2a3971f56fbecd8daa45965236d487683e13300e5c000e595bbadbbe9d0e1301fbbfa034f452abe8a4e4a427781f772370b9ff392bb03fc11ee43c9dee7  disable-broken-tests.patch
2c3aab4b24e0e1f60c3bd5aa2e24d93d3480711389e48454ed7cd7713eb27df7e1b29970ba1931ad0ae810458fae9e5cc7b6b6e2a24924eb4c493c6e7088ae75  postgresql.initd
a6d9cba5c7270484b3a22083b2b37742faefb01b6643040050c92235840c601b2e206ebda32804937b729c6cf42c79a558b921900e52fc420df2a03b5f29e1f7  postgresql.confd
f5a1cba051e7d846c2d16703514601cb25729ed96b677c9bd0c199d64552120a8b14b238af01917fdb87106681e12dee6fff7447558155ba273e4f96be5e2892  pg-restore.initd
c14a5684e914abb3b0ee71bbf15eed71a9264deacaa404a6e3af6bfc330d93e7598624d0ed11a94263106cc660f7f54c8ff57e759033cf606a795f69ff6c1c7c  pg-restore.confd
5c9bfd9e295dcf678298bf0aa974347a7c311d6e7c2aa76a6920fcb751d01fd1ab77abbec11f3c672f927ad9deaa88e04e370c0b5cd1b60087554c474b748731  pltcl_create_tables.sql"
