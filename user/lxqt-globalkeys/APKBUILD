# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=lxqt-globalkeys
pkgver=0.15.0
_lxqt_build=0.7.0
pkgrel=0
pkgdesc="Daemon and configuration utility for global shortcuts in LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt_build
	liblxqt-dev>=${pkgver%.*}.0 qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/lxqt-globalkeys/releases/download/$pkgver/lxqt-globalkeys-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="95beba4ce6c2324582d93a6f3f56d57681ec7d992db80a8d9fae34a637ba1612260932944e3865c3c749982e6aa111b3495558bc5d36e8b4c8e21027775e346e  lxqt-globalkeys-0.15.0.tar.xz"
