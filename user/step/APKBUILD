# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=step
pkgver=20.08.1
pkgrel=0
pkgdesc="Interactive physics simulation"
url="https://www.kde.org/applications/education/step/"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kdoctools-dev qt5-qtsvg-dev kcrash-dev khtml-dev kconfig-dev eigen-dev
	kdelibs4support-dev knewstuff-dev kplotting-dev gsl-dev
	libqalculate-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/step-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="91b28b5758bb43a28c7cc891c48edef3f8efe7821cf37cc501907c373d05ab40080d4e66199b436efc16c117063ab7d6951e9b3b3855d832e304935d6e7be4b7  step-20.08.1.tar.xz"
