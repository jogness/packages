# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gwenview
pkgver=20.08.1
pkgrel=0
pkgdesc="Fast and easy image viewer by KDE"
url="https://www.kde.org/applications/graphics/gwenview/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev phonon-dev
	kio-dev kactivities-dev kitemmodels-dev ki18n-dev kdoctools-dev
	kparts-dev kwindowsystem-dev kiconthemes-dev knotifications-dev
	libkipi-dev libjpeg-turbo-dev libpng-dev lcms2-dev zlib-dev exiv2-dev
	baloo-dev libkdcraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/gwenview-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	LDFLAGS="$LDFLAGS -Wl,-z,stack-size=1048576" cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="76382f3edb228667c85766049a4dad469496ef15364fbfec5b9c570cec0a7c6c083edd1832ccf82f8f7ed3c6d4819811cc2e784200db1a0298b9bc01ce606cb3  gwenview-20.08.1.tar.xz"
