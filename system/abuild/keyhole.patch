From 4f90ce92778d0ee302e288def75591b96a397c8b Mon Sep 17 00:00:00 2001
From: Max Rees <maxcrees@me.com>
Date: Thu, 13 Jun 2019 23:19:54 -0400
Subject: [PATCH] abuild-sudo: don't allow --keys-dir

Not allowing --allow-untrusted is obviously a good idea, but it can be
trivially bypassed if --keys-dir is allowed:

$ abuild-apk add foo-1-r0.apk
ERROR: foo-1-r0.apk: UNTRUSTED signature
$ abuild-apk --allow-untrusted add foo-1-r0.apk
abuild-apk: --allow-untrusted: not allowed option
$ cp -rp /etc/apk/keys /tmp/keys
$ cp untrusted.pub /tmp/keys
$ abuild-apk --keys-dir /tmp/keys add foo-1-r0.apk
(1/1) Installing foo (1-r0)
OK: 4319 MiB in 806 packages

If both --allow-untrusted and --keys-dir are not allowed, then it should
no longer be possible for an unprivileged member of the abuild group to
add an untrusted package.

$ abuild-apk --keys-dir /tmp/keys add foo-1-r0.apk
abuild-apk: --keys-dir: not allowed option
---
 abuild-sudo.c | 18 +++++++++++++++---
 1 file changed, 15 insertions(+), 3 deletions(-)

diff --git a/abuild-sudo.c b/abuild-sudo.c
index 80ceb8d7dd..12d64f13fd 100644
--- a/abuild-sudo.c
+++ b/abuild-sudo.c
@@ -32,6 +32,12 @@ static const char* valid_cmds[] = {
 	NULL
 };
 
+static const char* invalid_opts[] = {
+	"--allow-untrusted",
+	"--keys-dir",
+	NULL,
+};
+
 const char *get_command_path(const char *cmd)
 {
 	const char *p;
@@ -46,6 +52,14 @@ const char *get_command_path(const char *cmd)
 	return NULL;
 }
 
+void check_option(const char *opt)
+{
+	int i;
+	for (i = 0; invalid_opts[i] != NULL; i++)
+		if (strcmp(opt, invalid_opts[i]) == 0)
+			errx(1, "%s: not allowed option", opt);
+}
+
 int is_in_group(gid_t group)
 {
 	int ngroups_max = sysconf(_SC_NGROUPS_MAX) + 1;
@@ -105,10 +119,8 @@ int main(int argc, const char *argv[])
 	if (path == NULL)
 		errx(1, "%s: Not a valid subcommand", cmd);
 
-	/* we dont allow --allow-untrusted option */
 	for (i = 1; i < argc; i++)
-		if (strcmp(argv[i], "--allow-untrusted") == 0)
-			errx(1, "%s: not allowed option", "--allow-untrusted");
+		check_option(argv[i]);
 
 	argv[0] = path;
 	/* set our uid to root so bbsuid --install works */
